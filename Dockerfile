FROM bitnami/minideb:bookworm

ENV DEBIAN_FRONTEND noninteractive

RUN echo 'DEBIAN_FRONTEND=noninteractive' >> /etc/environment
RUN apt update && apt install -y devscripts git make debhelper sudo python3-virtualenv acl
RUN adduser --disabled-password --gecos packager packager
RUN usermod -aG sudo packager
RUN echo "packager ALL=(ALL) NOPASSWD:ALL" | EDITOR='tee -a' visudo
RUN sudo -u packager mkdir -p /home/packager/.virtualenvs
RUN git config --global --add safe.directory /app
