#!/bin/env bash

set -e

makedepends=(
    'help2man'
)

PACKAGE_NAME=cortile
REVISION=1
ARCHITECTURE=${ARCHITECTURE:-$(dpkg --print-architecture)}

build(){
    TEMPLATE_DIR=template

    # wget -q $(wget -qO- https://api.github.com/repos/leukipp/cortile/releases/latest | \
    # jq -r '.assets[] | select(.name | contains ("linux_amd64.tar.gz")) | .browser_download_url')
    LATEST=`curl -Ls -o /dev/null -w %{url_effective} \
        https://github.com/leukipp/cortile/releases/latest \
        | grep --only-matching '[^/]*$'` 
    LATEST_SEMVER=`echo "${LATEST}" | grep --only-matching -E "[0-9.]+"`

    # <name>_<version>-<revision>_<architecture>.deb
    PACKAGE_DIR="${PACKAGE_NAME}_${LATEST_SEMVER}-${REVISION}_${ARCHITECTURE}"
    rm -rf "${PACKAGE_DIR}"
    cp -r ${TEMPLATE_DIR} "${PACKAGE_DIR}"

    pushd ${PACKAGE_DIR}
    curl -L --output - "https://github.com/leukipp/cortile/releases/download/${LATEST}/cortile_${LATEST_SEMVER}_linux_amd64.tar.gz" |  tar -xvz
    mkdir -p usr/bin
    mv cortile usr/bin/
    mv README.md LICENSE usr/share/doc/cortile/

    mkdir -p usr/share/man/man1
    help2man --version-string="cortile ${LATEST_SEMVER}" \
        --section=1 \
        --name="Linux auto tiling manager for EWMH compliant window managers." \
        --no-discard-stderr usr/bin/cortile \
        --output=usr/share/man/man1/cortile.1
    gzip -fn9 usr/share/man/man1/cortile.1


    sed -i "s/@VERSION@/${LATEST_SEMVER}/" DEBIAN/control
    sed -i "s/@ARCH@/${ARCHITECTURE}/" DEBIAN/control

    sed -i "s/@VERSION@/${LATEST_SEMVER}/" usr/share/doc/cortile/changelog
    if [ -z $DIST_CODENAME ] 
    then 
        source /etc/os-release
        DIST_CODENAME=$VERSION_CODENAME
    fi
    sed -i "s/@DIST@/${DIST_CODENAME}/g" usr/share/doc/cortile/changelog
    sed -i "s/@DATE@/$(date -R)/g" usr/share/doc/cortile/changelog

    gzip --force --best -n usr/share/doc/cortile/changelog

    popd

    dpkg-deb --build --root-owner-group ${PACKAGE_DIR}
}

sudo apt install -y ${makedepends[*]}
(build)
