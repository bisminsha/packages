#!/bin/env bash

set -e

maintainer='Webyfy <info@webyfy.com>'
pkgname=sgx-software-enable
pkgver=1.0.1
pkgrel=1
pkgdesc='Enable Intel SGX on Linux systems.'
arch=amd64
section=utils
license=''
depends=()
makedepends=(
    'gcc'
    'checkinstall'
)
url="https://github.com/intel/sgx-software-enable"
src="https://github.com/intel/sgx-software-enable"
commit_id=7977d6d

prepare() {
	cd ${pkgname}/
    git apply ../make.patch
}

build() {
	cd ${pkgname}/
    make
}

package() {
	cd ${pkgname}/
	echo "${pkgdesc}" > description-pak
	sudo checkinstall -D -y \
	--install=no \
	--fstrans=no \
	--pkgname=${pkgname} \
	--pkgversion=${pkgver} \
	--pkgsource=${src} \
	--pkgarch=${arch} \
	--pakdir="../" \
	--pkgrelease=${pkgrel} \
	--pkggroup=${section} \
	--pkglicense=${license} \
	--maintainer="'${maintainer}'" \
	--requires="'$(IFS=,; echo "${depends[*]}")'" \
	--reset-uids=yes \
	--delspec=yes \
	--deldoc=yes \
	--deldoc=yes
	sudo make uninstall
}

main() {
	sudo apt install -y ${makedepends[*]}
	git clone "${src}"
	src_dir=$(basename ${src} .git)
	pushd ${src_dir}
	git checkout ${commit_id}
	popd

	(prepare)
	(build)
	(package)
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
    main "$@"
fi
