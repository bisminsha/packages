#!/bin/env bash
makedepends=(
    'checkinstall'
)
SRC_REPO=https://gitlab.com/webyfy/iot/e-gurukul/volctl.git

sudo apt install -y ${makedepends[*]}

git clone ${SRC_REPO} src
cd src
latest_tag=$(git describe --tags "$(git rev-list --tags --max-count=1)")
git checkout $latest_tag

sudo make deb
cp build/*.deb ../