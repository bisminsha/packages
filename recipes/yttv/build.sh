#!/bin/env bash

set -e

pkgname=yttv
makedepends=()
src="https://gitlab.com/webyfy/iot/e-gurukul/yttv"

prepare() {
    source /etc/os-release
    if [ $ID = "ubuntu" ] && $(dpkg --compare-versions "${VERSION_ID}" "lt" "22.04")
    then 
        sudo apt install -y software-properties-common
        sudo add-apt-repository ppa:jyrki-pulliainen/dh-virtualenv -y
    fi

    sudo apt install -y dh-virtualenv
    pip3 install -U help2man

    cd ${pkgname}/
    pip3 install -r requirements.txt
}

# sudo -E apt install -y ${makedepends[*]}
sudo apt install -y ${makedepends[*]}
git clone "${src}"
src_dir=$(basename ${src} .git)
pushd ${src_dir}
latest_tag=$(git describe --tags "$(git rev-list --tags --max-count=1)")
git checkout $latest_tag
popd

(prepare)
pushd ${src_dir}
chmod +x run
make deb
cp *.deb ../
popd
