#!/bin/env bash

RESET='\e[0m'
BOLD='\e[1m'
ITALIC='\e[3m'
RED='\e[31m'
GREEN='\e[32m'
YELLOW='\e[33m'
BLUE='\e[34m'

# LATEST=`curl -s https://api.github.com/repos/xfangfang/Macast/releases/latest \
#     | grep -oP '"tag_name": "\K(.*)(?=")'`
LATEST=`curl -Ls -o /dev/null -w %{url_effective} \
    https://github.com/xfangfang/Macast/releases/latest \
    | grep --only-matching '[^/]*$'` 
LATEST_SEMVER=`echo "${LATEST}" | grep --only-matching -E "[0-9.]+"`

echo -e "${GREEN}Download $LATEST release file...${RESET}"

# curl -s https://api.github.com/repos/xfangfang/Macast/releases/latest \
#   | grep browser_download_url \
#   | grep 'amd64.deb"' \
#   | cut -d '"' -f 4 \
#   | xargs -n 1 curl --fail -L -o macast-${LATEST_SEMVER}-amd64.deb \
#    ||  { echo -e "${RED}Failed to download release files${RESET}"; exit 1; }

ORIGINAL_DEB="macast-original.deb"
PACKAGE_DIR="macast-${LATEST_SEMVER}-amd64" 

curl --fail -L -o ${ORIGINAL_DEB} \
  https://github.com/xfangfang/Macast/releases/download/${LATEST}/Macast-Linux-${LATEST}-amd64.deb \
  || { echo -e "${RED}Failed to download release files${RESET}"; exit 1; }

dpkg-deb -R ${ORIGINAL_DEB} ${PACKAGE_DIR}
sed -i 's/Package: Macast/Package: macast/' macast-0.7-amd64/DEBIAN/control
rm ${ORIGINAL_DEB}
dpkg-deb --build --root-owner-group ${PACKAGE_DIR}
