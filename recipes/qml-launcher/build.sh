#!/bin/env bash

set -e

maintainer='Webyfy <info@webyfy.com>'
pkgname=qml-launcher
pkgver=0.1.2
pkgrel=1
pkgdesc='Desktop application launcher for Linux'
arch=amd64
section=x11
license=MIT
depends=(
    'qml-module-qtquick-controls2'
    'qml-module-qtquick-window2'
    'qml-module-qtquick2'
    'qml-module-qtquick-layouts'
)
makedepends=(
    'checkinstall'
    'qtdeclarative5-dev' 
    'g++'
)
url="https://github.com/alamminsalo/qml-launcher"
src="https://github.com/alamminsalo/qml-launcher"
commit_id=ba585e7

prepare() {
    cd ${pkgname}/
    git apply ../pro.diff
}

build() {
	cd ${pkgname}/
    qmake .
    make
}

package() {
    cd ${pkgname}
    echo "${pkgdesc}" > description-pak
    sudo checkinstall -D -y \
    --install=no \
    --fstrans=no \
    --pkgname=${pkgname} \
    --pkgversion=${pkgver} \
    --pkgsource=${src} \
    --pkgarch=${arch} \
    --pakdir="../" \
    --pkgrelease=${pkgrel} \
    --pkggroup=${section} \
    --pkglicense=${license} \
    --maintainer="'${maintainer}'" \
    --requires="'$(IFS=,; echo "${depends[*]}")'" \
    --reset-uids=yes \
    --delspec=yes \
    --deldoc=yes \
    --deldoc=yes
    sudo make uninstall
}

main() {
    sudo apt install -y ${makedepends[*]}
    git clone "${src}"
    src_dir=$(basename ${src} .git)
    pushd ${src_dir}
    git checkout ${commit_id}
    popd

    (prepare)
    (build)
    (package)
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
    main "$@"
fi
