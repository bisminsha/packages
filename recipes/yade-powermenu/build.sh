#!/bin/env bash

set -e

pkgname=yade-powermenu
makedepends=(
    g++
    cmake
    qttools5-dev
    qtdeclarative5-dev
    qtquickcontrols2-5-dev
)
src="https://gitlab.com/webyfy/iot/e-gurukul/yade-powermenu"

prepare() {
    :
}

sudo apt install -y ${makedepends[*]}
git clone "${src}"
src_dir=$(basename ${src} .git)
pushd ${src_dir}
latest_tag=$(git describe --tags "$(git rev-list --tags --max-count=1)")
git checkout $latest_tag
popd

(prepare)
pushd ${src_dir}
mkdir -p build
cd build
cmake ..
cpack -G DEB
cp *.deb ../../
popd
