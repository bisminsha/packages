#!/usr/bin/env bash

# Reference: https://www.baeldung.com/linux/create-debian-package

PACKAGE_NAME=mpv-handler
REVISION=1
ARCHITECTURE=amd64

TEMPLATE_DIR=template

LATEST_VERSION=`curl -Ls -o /dev/null -w %{url_effective} \
    https://github.com/akiirui/mpv-handler/releases/latest | \
    grep --only-matching '[^/]*$'` 
LATEST_SEMVER=`echo "${LATEST_VERSION}" | grep --only-matching -E "[0-9.]+"`

# <name>_<version>-<revision>_<architecture>.deb
PACKAGE_DIR="${PACKAGE_NAME}_${LATEST_SEMVER}-${REVISION}_${ARCHITECTURE}"
cp -r ${TEMPLATE_DIR} "${PACKAGE_DIR}"

curl --fail -L -O "https://github.com/akiirui/mpv-handler/releases/latest/download/mpv-handler-linux-amd64.zip"
unzip mpv-handler-linux-amd64.zip
rm mpv-handler-linux-amd64.zip

mkdir -p ${PACKAGE_DIR}/usr/{bin,share/applications}
mkdir -p ${PACKAGE_DIR}/etc/xdg/mpv-handler

mv README*.md ${PACKAGE_DIR}/usr/share/doc/mpv-handler/ 
mv *.desktop ${PACKAGE_DIR}/usr/share/applications/
mv mpv-handler ${PACKAGE_DIR}/usr/bin/
chmod +x ${PACKAGE_DIR}/usr/bin/mpv-handler
mv config.toml ${PACKAGE_DIR}/etc/xdg/mpv-handler/

sed -i "s/@VERSION@/${LATEST_SEMVER}/" ${PACKAGE_DIR}/DEBIAN/control
sed -i "s/@VERSION@/${LATEST_SEMVER}/" ${PACKAGE_DIR}/usr/share/doc/mpv-handler/changelog
if [ -z $DIST_CODENAME ] 
then 
    source /etc/os-release
    DIST_CODENAME=$VERSION_CODENAME
fi
sed -i "s/@DIST@/${DIST_CODENAME}/g" ${PACKAGE_DIR}/usr/share/doc/mpv-handler/changelog
sed -i "s/@DATE@/$(date -R)/g" ${PACKAGE_DIR}/usr/share/doc/mpv-handler/changelog

gzip --best -n ${PACKAGE_DIR}/usr/share/doc/mpv-handler/changelog
gzip --best -n ${PACKAGE_DIR}/usr/share/man/man1/mpv-handler.1

dpkg-deb --build --root-owner-group ${PACKAGE_DIR}