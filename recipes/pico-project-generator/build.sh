#!/bin/env bash

set -e

makedepends=(
    'python3-pip'
)
src="https://gitlab.com/quna/python-packaging"
src_dir="src"

prepare() {
    source /etc/os-release
    if [ $ID = "ubuntu" ] && $(dpkg --compare-versions "${VERSION_ID}" "lt" "22.04")
    then 
        sudo apt install -y software-properties-common
        sudo add-apt-repository ppa:jyrki-pulliainen/dh-virtualenv -y
    fi

    sudo apt install -y dh-virtualenv
    pip3 install -U pipreqs
    cd ${src_dir}
    git submodule update --init --recursive
}

sudo apt install -y ${makedepends[*]}
git clone "${src}" ${src_dir}
pushd ${src_dir}
latest_tag=$(git describe --tags "$(git rev-list --tags --max-count=1)")
git checkout $latest_tag
popd

(prepare)
pushd ${src_dir}/example
make deb
cp ../*.deb ../../
popd
